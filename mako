background-color=#{{ theme.background.default }}
text-color=#{{ theme.foreground }}
border-color=#{{ theme.background.dark }}
font={{ font.monospace }} {{ font.size }}
layer=overlay
max-visible=3
format=<b>%a</b> · %s\n%b
default-timeout=5000
width=500
height=150

[urgency=high]
border-color=#{{ theme.dark.red }}
