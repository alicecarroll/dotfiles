from traitlets.config import get_config

c = get_config()
c.TerminalInteractiveShell.true_color = True
c.TerminalInteractiveShell.highlighting_style = "gruvbox"
