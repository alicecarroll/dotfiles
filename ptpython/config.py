__all__ = ("configure",)


def configure(repl):
    repl.show_signature = True
    repl.show_docstring = True
    repl.vi_mode = True
    repl.prompt_style = "classic"
    repl.complete_while_typing = True
    repl.enable_fuzzy_completion = True
    repl.enable_dictionary_completion = True
    repl.show_line_numbers = True
    repl.highlight_matching_parenthesis = True
    repl.enable_auto_suggest = True
    repl.color_depth = "DEPTH_24_BIT"
    repl.use_code_colorscheme("monokai")
